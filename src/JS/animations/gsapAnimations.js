'use strict';

import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";
// import { Timeline, CSSPlugin } from "gsap/gsap-core";
import Splitting from "splitting";

Splitting();
gsap.registerPlugin(ScrollTrigger);
// gsap.registerPlugin(CSSPlugin);
// gsap.registerPlugin(Timeline);

const H1 = document.querySelector('.hero__header--title')
const CHAR = document.querySelectorAll('.char')
const SECTION = document.querySelectorAll('section')
const WRAPPER = document.querySelectorAll('.page__wrapper')

console.log(WRAPPER)

CHAR.forEach(char => {
    gsap.fromTo(char, 
        {
            y: '+=100',
            opacity: 0
        }, 
        {
            y: 0, 
            opacity: 1,
            duration: 4,
            ease: 'easeInOut'
        }  
    )});

