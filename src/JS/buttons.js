// TO TOP BUTTON SCROLL
const toTopButton = document.getElementById('toTop');

function scrollToTop() {
   if (window.scrollY !== 0) {
     window.requestAnimationFrame(scrollToTop);
     window.scrollTo(0, window.scrollY - window.scrollY / 8);
   }
}
 
toTopButton.addEventListener('click', function(ev) {
   ev.preventDefault();
   scrollToTop();
});

// TOGGLE CLASS BASED ON PAGE POSITION

function toggleToTopButton() {
   if (window.scrollY > 300) {
      toTopButton.classList.add('--toTop-visible');
     } else {
      toTopButton.classList.remove('--toTop-visible');
    }
}
    
window.addEventListener('scroll', toggleToTopButton);
