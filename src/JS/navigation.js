const menu = document.querySelector('.menu');
const navLink = document.querySelectorAll('.menu__link');
const hamburger = document.querySelector('.hamburger')

hamburger.addEventListener('click', event => {
    event.preventDefault();
    hamburger.classList.toggle('is-active')
    menu.classList.toggle('--menu-toggle');

    if (menu.classList.contains('--menu-toggle')) {
      document.body.classList.add('page__body--with-menu');
      document.body.style.removeProperty('overflow-x');
    } else {
      document.body.classList.remove('page__body--with-menu');
      document.body.style.addProperty('overflow-x');
    }
  });

 
navLink.forEach(link => link.addEventListener('click', e => {
    menu.classList.remove('--menu-toggle');
    document.body.classList.remove('page__body--with-menu');
    hamburger.classList.toggle('is-active')
  }));
